This is Byspectrum, a library for the Large Scale Structure bispectrum.

  Copyright 2018 Francesco Montanari, Enea Di Dio

  Copying and distribution of this file, with or without modification,
  are permitted in any medium without royalty provided the copyright
  notice and this notice are preserved.

Home page: https://gitlab.com/montanari/byspectrum

Byspectrum is free software.  See the file COPYING for copying conditions.

# Description

Byspectrum computes the dominant tree-level angular bispectrum of the
Large Scale Structure.

*This* **alpha release** *of Byspectrum is meant to guarantee
reproducible results for the work cited at the end of this file. The
code still misses important features (most notably some lensing
magnification term and redshift window functions).*

# Setup

## Dependencies

The software is written in Python 3 and depends on

- `numpy`
- [`CosmoX v0.1`](https://gitlab.com/montanari/cosmox). *CosmoX (a C++
  library for the computation of generalized angular spectra and
  bispectra with a Python 3 interface) is only at a beta release
  stage. While CosmoX provides a standard build/installation
  toolchain, it depends itself on external libraries without
  installation scripts that require ad-hoc configuration (see its
  README). Also, future releases of CosmoX (versions > 0.1) are likely
  to introduce breaking changes.*

## Download

https://gitlab.com/montanari/byspectrum/

## Installation

```shell
python3 setup.py build
python3 setup.py install
```

(Check out the `install.sh` and `uninstall.sh` scripts for a local
installation.)

# Documentation

Documentation is automatically generated with Sphinx. Build the
documentation:

```shell
cd docs/
make html
```

See the [demo](demo/) folder for usage examples.

# Cite

If you use this code in a scientific publication, please cite the
following work:

- E. Di Dio, R. Durrer, R. Maartens, F. Montanari, and O. Umeh, *The
  Full-Sky Angular Bispectrum in Redshift Space*,
  [arXiv:1812.09297](https://arxiv.org/abs/1812.09297)
  [astro-ph.CO]. [[BibTex](https://inspirehep.net/record/1711173/export/hx)]
