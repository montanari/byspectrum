Lensing reduced bispectrum
==========================

.. automodule:: byspectrum.b_lensing
    :members:
    :undoc-members:
    :show-inheritance:
