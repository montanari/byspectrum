Redshift-space distortions (RSD) reduced bispectrum
===================================================

.. automodule:: byspectrum.b_rsd
    :members:
    :undoc-members:
    :show-inheritance:
