Introduction
============

This documentation is automatically generated from Byspectrum source
docstrings.

Byspectrum
----------

The code computes the tree level Large Scale Structure bispectrum.

A note about cached results
---------------------------

Single bispectrum permutations are calculated using `lru_cache`
memoization to automatically avoid unnecessary computations in the
total result. For instance, let's consider the following cases:

- All redshifts and multipoles are equal. Then all permutation terms
  are equal and only one permutation needs to be actually computed.

- Call separate terms first, and then the total reduced bispectrum
  with same arguments. Since the separate terms have been already
  computed, they won't be calculated again from scratch for the
  total bispectrum.
