.. Byspectrum documentation master file, created by
   sphinx-quickstart on Wed Nov 28 10:33:37 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Byspectrum's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   b_density
   b_lensing
   b_rsd
   b_velocity
   covariance
   galaxy_bias

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
