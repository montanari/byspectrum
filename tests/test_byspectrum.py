# Copyright (C) 2018 Francesco Montanari.

# This file is part of byspectrum.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Byspectrum unit test."""

import sys
import unittest

import cosmox

from byspectrum.b_density import b_density
from byspectrum.b_rsd import b_rsd
from byspectrum.b_velocity import b_vv2, b_v1v1, b_vd1, b_v1d
from byspectrum.b_lensing import b_NdNpsi, b_Nv1Npsi

class RegressionTest(unittest.TestCase):
    """Test for regression bugs. So far only test an equal redshifts and
    equal multipoles configuration. Need to extend the tests.
    """

    def setUp(self):
        self.pt = cosmox.Perturbations_create_perturbs(cosmox.kCosmoclass,
                                                       "cosmoclass.ini")
        self.gsp = cosmox.GeneralSpectra(self.pt)
        self.bisps = [b_density, b_rsd, b_vv2, b_v1v1, b_vd1, b_v1d,
                      b_NdNpsi, b_Nv1Npsi]

    def _compute_bisp(self, bisp, l1, l2, l3, z1, z2, z3, pt, gsp,
                      epsrel=1e-3, epsabs=0.):
        """For speed use Cquad integrator for lensing. Need to extend tests
        also to kQag lensing integrator. Similarly, need to extend
        tests to kCquad integrator for other cases.
        """
        integtype = 'kCquad' if (bisp in [b_NdNpsi, b_Nv1Npsi]) else 'kMix'
        if bisp is b_rsd:
            return bisp(l1, l2, l3, z1, z2, z3, pt, gsp, epsrel=epsrel,
                        epsabs=epsabs, integtype=integtype)
        else:
            return bisp(l1, l2, l3, z1, z2, z3, gsp, epsrel=epsrel,
                        epsabs=epsabs, integtype=integtype)

    def test_same_lz(self):
        z1 = z2 = z3 = 0.5
        l1 = l2 = l3 = 100
        expected = [8.498451185392587e-08, 4.100165664317421e-08,
                    9.502063214469567e-13, 2.544045029721716e-08,
                    8.387972942129662e-13, 6.051653929737791e-08,
                    2.4690601750962576e-12, 1.0313171964822572e-12]
        for i, bisp in enumerate(self.bisps):
            sys.stdout.write(bisp.__name__ + '\n')
            res = self._compute_bisp(bisp, l1, l2, l3, z1, z2, z3,
                                     self.pt, self.gsp)
            self.assertAlmostEqual(res, expected[i])

if __name__ == '__main__':
    unittest.main()
