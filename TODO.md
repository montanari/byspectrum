- Add more tests.

- Refactor modules structure: create a `reduced_bispectrum` module
  listing all reduced bispectra (in particular, remove redundant
  `b_density.b_density`, `b_rsd.b_rsd`).

- Add different redshifts covariance.

- Add window functions.
