from setuptools import setup, find_packages

import byspectrum

setup(name='byspectrum',
      version=byspectrum.__version__,
      description='Large Scale Structure bispectrum',
      classifiers=[
          "Development Status :: 3 - Alpha",
          "Intended Audience :: Science/Research",
          "Programming Language :: Python :: 3",
      ],
      author='Francesco Montanari',
      author_email='fmnt@fmnt.info',
      url='https://gitlab.com/montanari/byspectrum',
      license='GPL3+',
      packages=find_packages(),
)
