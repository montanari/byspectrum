# Copyright (C) 2018 Francesco Montanari.

# This file is part of byspectrum.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Decorator to compute all bispectrum permutations."""

import functools

def permute(func):
    """Decorator: declare @permute before defining a bispectrum-like
    function in the form `func(l1, l2, l3, z1, z2, z3, *args)`, where
    `*args` are further optional arguments. All angular bispectrum
    permutations are added when calling the function so defined,
    assuming the following form for the bispectrum:

      <A(l1,z1)B(l2,z2)B(l3,z3)> + <B(l1,z1)A(l2,z2)B(l3,z3)>
      + <B(l1,z1)B(l2,z2)A(l3,z3)>
    """
    @functools.wraps(func)
    def permuted(l1, l2, l3, z1, z2, z3, *args, **kwargs):
        return (func(l1, l2, l3, z1, z2, z3, *args, **kwargs)
                + func(l2, l3, l1, z2, z3, z1, *args, **kwargs)
                + func(l3, l1, l2, z3, z1, z2, *args, **kwargs))
    return permuted
