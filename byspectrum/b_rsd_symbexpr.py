# Copyright (C) 2018 Francesco Montanari.

# This file is part of byspectrum.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Redshift-space distortions expression for separable
terms. Expression generated automatically, not meant to be
human-readable.

Note: in the main code it is assumed that the linear RSD are
introduced via the transfer function related to the first redshift
index of generalized spectra. E.g., given `CL2(z2,z1)*CL3m2(z3,z1)',
linear RSD are introduced in the transfers computed at `z2, z3'.
"""

def b_rsd_symbexpr(L1, L2, L3, z1, z2, z3, CL2, CL3, CL2m2, CL2p2,
                   CL3m2, CL3p2, m1CL2m1, m1CL2p1, m1CL3m1, m1CL3p1,
                   p1CL2m1, p1CL2m3, p1CL2p1, p1CL2p3, p1CL3m1,
                   p1CL3m3, p1CL3p1, p1CL3p3):
    return (((4*(-115 + 8*L1*(1 + L1) + 88*L2*(1 + L2)))/(-3 + 4*L2*(1 + L2)) + (16*L3**2)/(-3 + 4*L2*(1 + L2)) + (-49 + 8*L1*(1 + L1) + 4*L2**2)/(-1 + 2*L3) + (49 - 8*L1*(1 + L1) - 4*L2**2)/(3 + 2*L3))*CL2(z2,z1)*CL3(z3,z1))/224. + ((3 - 3*L2*(13 + 27*L2) + 4*L1*(1 + L1)*(1 + 2*L2**2) + 9*L3 + 2*(-4*L1*(1 + L1) + L2*(29 + 22*L2))*L3 + (19 - 8*L1*(1 + L1) + 44*L2*(1 + L2))*L3**2 - 8*L2*L3**3 - 4*L3**4)*CL2m2(z2,z1)*CL3(z3,z1))/(28.*(-1 + 4*L2**2)*(-3 + 4*L3*(1 + L3))) + (((2 - 4*L1*(1 + L1) + 2*L2*(9 + 11*L2))/(3 + 4*L2*(2 + L2)) + (2*L3)/(1 + 2*L2) - (2*L3**2)/(3 + 4*L2*(2 + L2)) + ((-2 + L1)*(3 + L1))/(-1 + 2*L3) - ((-2 + L1)*(3 + L1))/(3 + 2*L3))*CL2p2(z2,z1)*CL3(z3,z1))/56. + (((2*(-81 + 8*L1*(1 + L1) + 44*L2*(1 + L2)))/(-3 + 4*L2*(1 + L2)) + (49 - 8*L1*(1 + L1) - 4*L2**2)/(-1 + 2*L3) + (3 + 8*L1*(1 + L1) + 4*(-2 + L2)*L2)/(1 + 2*L3))*CL2(z2,z1)*CL3m2(z3,z1))/224. + ((-9 - 4*(-3 + 2*L1*(1 + L1))*(-1 + L3)*L3 - 4*L2*(-3 + 2*L1*(1 + L1) - 6*L3)*(-1 + 2*L3) + L2**2*(-8*L1*(1 + L1) + 12*(1 + 2*L3)**2))*CL2m2(z2,z1)*CL3m2(z3,z1))/(56.*(-1 + 4*L2**2)*(-1 + 4*L3**2)) + ((-8*L1*(1 + L2 - L3)*(2 + L2 - L3) - 8*L1**2*(1 + L2 - L3)*(2 + L2 - L3) + 3*(1 + 2*L2)*(1 + 2*L3)*(5 + 2*L3 + L2*(2 + 4*L3)))*CL2p2(z2,z1)*CL3m2(z3,z1))/(56.*(3 + 4*L2*(2 + L2))*(-1 + 4*L3**2)) + (((2*(-81 + 8*L1*(1 + L1) + 44*L2*(1 + L2)))/(-3 + 4*L2*(1 + L2)) - (3 + 8*L1*(1 + L1) + 4*(-2 + L2)*L2)/(1 + 2*L3) + (-49 + 8*L1*(1 + L1) + 4*L2**2)/(3 + 2*L3))*CL2(z2,z1)*CL3p2(z3,z1))/224. + ((-8*L1*(-2 + L2 - L3)*(-1 + L2 - L3) - 8*L1**2*(-2 + L2 - L3)*(-1 + L2 - L3) + 3*(1 + 2*L2)*(1 + 2*L3)*(5 + 2*L3 + L2*(2 + 4*L3)))*CL2m2(z2,z1)*CL3p2(z3,z1))/(56.*(-1 + 4*L2**2)*(3 + 4*L3*(2 + L3))) + ((-8*L1*(2 + L2 + L3)*(3 + L2 + L3) - 8*L1**2*(2 + L2 + L3)*(3 + L2 + L3) + 3*(1 + 2*L2)*(1 + 2*L3)*(-3 + 2*L3 + L2*(2 + 4*L3)))*CL2p2(z2,z1)*CL3p2(z3,z1))/(56.*(1 + 2*L2)*(3 + 2*L2)*(1 + 2*L3)*(3 + 2*L3)) + ((19 + 10*L2 + 24*L3)*m1CL3m1(z3,z1)*p1CL2m1(z2,z1))/(112 + 224*L3) + ((5 - 10*L2 + 24*L3)*m1CL3p1(z3,z1)*p1CL2m1(z2,z1))/(112 + 224*L3) + ((3 + 2*L2 + 8*L3)*m1CL3m1(z3,z1)*p1CL2m3(z2,z1))/(112 + 224*L3) + ((5 - 2*L2 + 8*L3)*m1CL3p1(z3,z1)*p1CL2m3(z2,z1))/(112 + 224*L3) + ((17 - 10*L2 + 16*L3)*m1CL3m1(z3,z1)*p1CL2p1(z2,z1))/(112 + 224*L3) + ((-1 + 10*L2 + 16*L3)*m1CL3p1(z3,z1)*p1CL2p1(z2,z1))/(112.*(1 + 2*L3)) + ((1 - 2*L2 + 8*L3)*m1CL3m1(z3,z1)*p1CL2p3(z2,z1))/(112 + 224*L3) + ((7 + 2*L2 + 8*L3)*m1CL3p1(z3,z1)*p1CL2p3(z2,z1))/(112 + 224*L3) + ((19 + 24*L2 + 10*L3)*m1CL2m1(z2,z1)*p1CL3m1(z3,z1))/(112 + 224*L2) + ((5 + 24*L2 - 10*L3)*m1CL2p1(z2,z1)*p1CL3m1(z3,z1))/(112 + 224*L2) + ((3 + 8*L2 + 2*L3)*m1CL2m1(z2,z1)*p1CL3m3(z3,z1))/(112 + 224*L2) + ((5 + 8*L2 - 2*L3)*m1CL2p1(z2,z1)*p1CL3m3(z3,z1))/(112 + 224*L2) + ((17 + 16*L2 - 10*L3)*m1CL2m1(z2,z1)*p1CL3p1(z3,z1))/(112 + 224*L2) + ((-1 + 16*L2 + 10*L3)*m1CL2p1(z2,z1)*p1CL3p1(z3,z1))/(112.*(1 + 2*L2)) + ((1 + 8*L2 - 2*L3)*m1CL2m1(z2,z1)*p1CL3p3(z3,z1))/(112 + 224*L2) + ((7 + 8*L2 + 2*L3)*m1CL2p1(z2,z1)*p1CL3p3(z3,z1))/(112 + 224*L2)
