# Copyright (C) 2018 Francesco Montanari.

# This file is part of byspectrum.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Bispectrum covariance."""

import warnings

import cosmox
import numpy as np

def _spectra(gsp, ell, z1, z2, ttype1, ttype2, epsrel=1e-4, epsabs=0.):
    """Compute standard spectra based on the given transfer types.
       The numerical integration estimated relative precision `epsrel`
       is an optional parameter.
    """
    n = 0
    l1 = l2 = int(ell) # Avoid incorrect type errors.
    return gsp.compute(l1, l2, z1, z2, n, ttype1, ttype2, epsrel,
                       epsabs).real

def _kronecker(i, j):
    """Kronecker delta."""
    if i==j:
        return 1
    else:
        return 0

def B_dens_cosmic_variance(l1, l2, l3, z, gsp, linear_rsd=False,
                           epsrel=1e-4, epsabs=0.):
    """Equal redshifts cosmic variance of density angle-averaged
    bispectrum, Gaussian approximation. Adapted from equation (3.23)
    of arXiv:astro-ph/0206039.

    Parameters
    ----------
    l1 : int
        Multipole.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z : float
        Redshift.
    gsp : object
        Instance of cosmox.GeneralSpectra.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Relative tolerance for spectra computation.
    epsabs : float
        Absolute tolerance for spectra computation.
    """
    if any(np.array([l1, l2, l3]) <= 0):
        raise RuntimeError("Variance requires positive multipoles")
    if ((l1+l2+l3) % 2) != 0:
        warnings.warn("Triangle condition violated, returning null variance",
                      RuntimeWarning)
        return 0.0
    multiplicity = (1.0 + 2.0*_kronecker(l1, l2)*_kronecker(l2, l3) +
                    _kronecker(l1, l2) + _kronecker(l2, l3) +
                    _kronecker(l3, l1))
    ttype = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    if not linear_rsd:
        galbias = gsp.get_galaxy_bias()
        multiplicity  *= galbias.b1(z)**6.
    return ((_spectra(gsp, l1, z, z, ttype, ttype, epsrel=epsrel,
                      epsabs=epsabs)
             * _spectra(gsp, l2, z, z, ttype, ttype, epsrel=epsrel,
                        epsabs=epsabs)
             * _spectra(gsp, l3, z, z, ttype, ttype, epsrel=epsrel,
                        epsabs=epsabs))
            * multiplicity)
