# Copyright (C) 2018 Francesco Montanari.

# This file is part of byspectrum.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Redshift-space distortions reduced bispectra."""

from functools import lru_cache

import cosmox
import numpy as np

from .b_density import _set_integtype, _set_multipoles
from .b_rsd_symbexpr import b_rsd_symbexpr
from .permute import permute

@lru_cache()
def _spectrum(gsp, l1, l2, z1, z2, n, ttype_A, ttype_B, epsrel,
              epsabs, integtype):
    """Memoized general spectra."""
    integtype = _set_integtype(integtype)
    return gsp.compute(l1, l2, z1, z2, n, ttype_A, ttype_B, epsrel,
                       epsabs, integtype)

def _partial_spectrum(gsp, l1, l2, n, ttype_A, ttype_B, epsrel, epsabs,
                      integtype):
    """Wrap general spectra and return a function of redshifts only."""
    def func(z1, z2):
        return _spectrum(gsp, l1, l2, z1, z2, n, ttype_A, ttype_B,
                         epsrel, epsabs, integtype)
    return func

def _get_arguments(l1, l2, l3, z1, z2, z3, gsp, ttype_A, ttype_B,
                   epsrel, epsabs, integtype):
    """Return tuple containing arguments for the symbolic expression."""
    CL2 = _partial_spectrum(gsp, l2, l2, 0, ttype_A, ttype_B, epsrel,
                            epsabs, integtype)
    CL3 = _partial_spectrum(gsp, l3, l3, 0, ttype_A, ttype_B, epsrel,
                            epsabs, integtype)
    CL2m2 = _partial_spectrum(gsp, l2, l2-2, 0, ttype_A, ttype_B,
                              epsrel, epsabs, integtype)
    CL2p2 = _partial_spectrum(gsp, l2, l2+2, 0, ttype_A, ttype_B,
                              epsrel, epsabs, integtype)
    CL3m2 = _partial_spectrum(gsp, l3, l3-2, 0, ttype_A, ttype_B,
                              epsrel, epsabs, integtype)
    CL3p2 = _partial_spectrum(gsp, l3, l3+2, 0, ttype_A, ttype_B,
                              epsrel, epsabs, integtype)
    m1CL2m1 = _partial_spectrum(gsp, l2, l2-1, -1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    m1CL2p1 = _partial_spectrum(gsp, l2, l2+1, -1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    m1CL3m1 = _partial_spectrum(gsp, l3, l3-1, -1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    m1CL3p1 = _partial_spectrum(gsp, l3, l3+1, -1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    p1CL2m1 = _partial_spectrum(gsp, l2, l2-1, 1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    p1CL2m3 = _partial_spectrum(gsp, l2, l2-3, 1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    p1CL2p1 = _partial_spectrum(gsp, l2, l2+1, 1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    p1CL2p3 = _partial_spectrum(gsp, l2, l2+3, 1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    p1CL3m1 = _partial_spectrum(gsp, l3, l3-1, 1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    p1CL3m3 = _partial_spectrum(gsp, l3, l3-3, 1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    p1CL3p1 = _partial_spectrum(gsp, l3, l3+1, 1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    p1CL3p3 = _partial_spectrum(gsp, l3, l3+3, 1, ttype_A, ttype_B,
                                epsrel, epsabs, integtype)
    return (l1, l2, l3, z1, z2, z3, CL2, CL3, CL2m2, CL2p2, CL3m2,
            CL3p2, m1CL2m1, m1CL2p1, m1CL3m1, m1CL3p1, p1CL2m1,
            p1CL2m3, p1CL2p1, p1CL2p3, p1CL3m1, p1CL3m3, p1CL3p1,
            p1CL3p3)

@permute
@lru_cache()
def b_rsd_separable(l1, l2, l3, z1, z2, z3, pt, gsp, linear_rsd=False,
                    epsrel=1e-4, epsabs=0., integtype='kMix'):
    """Compute RSD reduced bispectrum neglecting non-separable monopole
    term. Include all permutations.

    Parameters
    ----------
    l1 : int
        Multipoles.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    pt : object
        CosmoX Perturbations instance.
    gsp : object
        CosmoX GeneralSpectra instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.
    """
    ttype = gsp.kDensity
    ttype_G = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    # Assume that linear RSD are introduced in the transfer function
    # related to the first redshift argument of generalized spectra
    # (i.e., `z2, z3' for this permutation). This must be consistent
    # with the symbolic expression in `b_rsd_sybexpr.py'.
    args = _get_arguments(l1, l2, l3, z1, z2, z3, gsp, ttype_G,
                          ttype, epsrel, epsabs, integtype)
    b_rsd = b_rsd_symbexpr(*args)
    if b_rsd.imag != 0:
        raise RuntimeError("Result {} should be real number".format(sp))
    galbias = gsp.get_galaxy_bias()
    b_rsd = 2. * pt.bg_velocity_growth_factor(z1) * b_rsd.real
    if not linear_rsd:
        b_rsd *= galbias.b1(z2) * galbias.b1(z3)
    return b_rsd

def b_rsd_nonseparable(l1, l2, l3, z1, z2, z3, pt, galbias,
                       linear_rsd=False, epsrel=1e-4, epsabs=0.,
                       maxeval=5e6, ignore_fail=True,
                       peak_width_scale=10.):
    """Compute reduced bispectrum from the non-separable RSD monopole term
    only. Include all permutations.

    Parameters
    ----------
    l1 : int
        Multipoles.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    pt : object
        CosmoX Perturbations instance.
    galbias : object
        CosmoX GalaxyBias instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    maxeval : int
        Maximum number of integrand evaluation. Only relevant for
        non-separable contribution.
    peak_width_scale : float > 1.0
        Line-of-sight integral boundary width scale factor (around the
        integrand peak). If set to `numpy.inf`, integrate over [0, inf].
        Only relevant for non-separable contribution.
    """
    brsd = cosmox.BispectrumRsd(pt, galbias, linear_rsd, epsrel,
                                epsabs, maxeval, ignore_fail)
    return brsd.get_nonseparable(l1, l2, l3, z1, z2, z3,
                                 peak_width_scale)

def b_rsd(l1, l2, l3, z1, z2, z3, pt, gsp, linear_rsd=False,
          epsrel=1e-4, epsabs=0., integtype='kMix', maxeval=5e6,
          peak_width_scale=10.):
    """Compute RSD reduced bispectrum, including all permutations.

    Differently from other reduced bispectra, RSD also requires a
    CosmoX perturbations instance to be passed as input. This is due
    to the fact that the non-searable RSD bispectrum component is not
    expressed in terms of generalized spectra.

    .. note: If the non-separable integral contribution fails, it will
       *not* return error. Convergence must be checked varying
       `maxeval'.

    Parameters
    ----------
    l1 : int
        Multipoles.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    pt : object
        Instance of cosmox.Perturbations.
    gsp : object
        Instance of cosmox.GeneralSpectra.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. Only affects separable
        contribution. *Warning:* The absolute tolerance refers to the
        integral defined in the C++ code up to some pre-factor.
    integtype : str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.
    maxeval : int
        Maximum number of integrand evaluation. Only relevant for
        non-separable contribution.
    peak_width_scale : float > 1.0
        Line-of-sight integral boundary width scale factor (around the
        integrand peak). If set to `numpy.inf`, integrate over [0, inf].
        Only relevant for non-separable contribution.

    """
    l1, l2, l3 = _set_multipoles(l1, l2, l3)
    maxeval = int(maxeval)
    brsd_s = b_rsd_separable(l1, l2, l3, z1, z2, z3, pt, gsp,
                             linear_rsd=linear_rsd, epsrel=epsrel,
                             epsabs=epsabs, integtype=integtype)
    # We'd need two different epsabs, for separable and non-separable
    # contributions. However, in the case of the non-separable term it
    # seems safer to keep a zero value.
    brsd_ns = b_rsd_nonseparable(l1, l2, l3, z1, z2, z3, pt,
                                 gsp.get_galaxy_bias(),
                                 linear_rsd=linear_rsd,
                                 epsrel=epsrel, epsabs=0.,
                                 maxeval=maxeval,
                                 peak_width_scale=peak_width_scale)
    return brsd_s + brsd_ns
