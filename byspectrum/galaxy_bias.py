# Copyright (C) 2018 Francesco Montanari.

# This file is part of byspectrum.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Clustering bias."""

import cosmox

class HiBias(cosmox.GalaxyBias):
    """HI intensity mapping bias.

    .. warning: This class methods are called iteratively several
       times by the computationally expensive RSD bispectrum
       integrand, hence they should be optimized as much as possible
       to avoid slowing down terribly computations.
    """

    def __init__(self):
        super().__init__()

    def _check_polyfit_range(self, z):
        if (z<1.025272e-02) or (z>5.0):
            raise ValueError("Redshift outside HI bias interpolation table")

    def b1(self, z):
        """Linear bias. Fit Jolicoeur et al. (in preparation) results.

        Parameters
        ----------
        z : float
            Redshift.
        """
        self._check_polyfit_range(z)
        return -0.0022*z**3. + 0.032*z**2. + 0.071*z + 0.7531

    def b2(self, z):
        """Quadratic bias. Fit Jolicoeur et al. (in preparation) results.

        Parameters
        ----------
        z : float
            Redshift.
        """
        self._check_polyfit_range(z)
        return 0.0056*z**3. - 0.0068*z**2. - 0.0631*z - 0.2569

    def bS(self, z):
        """Tidal bias.

        Parameters
        ----------
        z : float
            Redshift.
        """
        self._check_polyfit_range(z)
        return -2./7.*(self.b1(z)-1.)

class EuclidBias(cosmox.GalaxyBias):
    """Euclid (spectro-z) galaxy bias.

    Fit to data in Table 1, Yankelevich and Porciani arXiv:1807.07076.

    .. warning: This class methods are called iteratively several
       times by the computationally expensive RSD bispectrum
       integrand, hence they should be optimized as much as possible
       to avoid slowing down terribly computations.
    """

    def __init__(self):
        super().__init__()

    def _check_polyfit_range(self, z):
        if (z<0.7) or (z>2.0):
            raise ValueError("Redshift outside HI bias interpolation table")

    def b1(self, z):
        """Linear bias.

        Parameters
        ----------
        z : float
            Redshift.
        """
        self._check_polyfit_range(z)
        return 0.4*z + 0.9

    def b2(self, z):
        """Quadratic bias.

        Parameters
        ----------
        z : float
            Redshift.
        """
        self._check_polyfit_range(z)
        return (-0.00771288*z**3. + 0.18302286*z**2. - 0.20799274*z -
                0.70417186)

    def bS(self, z):
        """Tidal bias.

        To recover tidal bias numbers in arXiv:1807.07076, our
        definition must be multiplied by 2.

        Parameters
        ----------
        z : float
            Redshift.

        """
        return -2./7.*(self.b1(z)-1.)
