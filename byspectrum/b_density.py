# Copyright (C) 2018 Francesco Montanari.

# This file is part of byspectrum.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Density reduced bispectra."""

from functools import lru_cache
import math

import cosmox

from .permute import permute

def _set_integtype(integtype):
    "Convert str to cosmox::GeneralSpectra::IntegType."
    ldict = locals()            # https://bugs.python.org/issue4831
    exec('it = cosmox.GeneralSpectra.' + integtype, globals(), ldict)
    it = ldict['it']
    return it

def _set_multipoles(l1, l2, l3):
    """Force valid values."""
    if (l1<3) or (l2<3) or (l3<3):
        raise ValueError("Multipoles must be l>=3")
    return int(l1), int(l2), int(l3)

@lru_cache()
def b_dens_monopole_oneperm(l1, l2, l3, z1, z2, z3, gsp,
                            linear_rsd=False, epsrel=1e-4, epsabs=0.,
                            integtype='kMix'):
    """Compute one permutation of the density monopole reduced bispectrum.

    This function requires second order bias to be defined in the
    `cosmox.GalaxyBias` object used to instantiate `gsp`.

    Parameters
    ----------
    l1 : int
        Multipole.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    gsp : object
        CosmoX GeneralSpectra instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.
    """
    l1, l2, l3 = _set_multipoles(l1, l2, l3)
    integtype = _set_integtype(integtype)
    ttype = gsp.kDensity
    ttype_G = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    n = 0
    res = (gsp.compute(l1, l1, z1, z3, n, ttype_G, ttype,
                       epsrel, epsabs, integtype)
           * gsp.compute(l2, l2, z2, z3, n, ttype_G, ttype,
                         epsrel, epsabs, integtype))
    galbias = gsp.get_galaxy_bias()
    res *= (34. / 21. * galbias.b1(z3) + galbias.b2(z3))
    if not linear_rsd:
        res *= galbias.b1(z1)*galbias.b1(z2) # gsp.kDensity has no bias.
    return res.real

@lru_cache()
def b_dens_dipole_oneperm(l1, l2, l3, z1, z2, z3, gsp,
                          linear_rsd=False, epsrel=1e-4, epsabs=0.,
                          integtype='kMix'):
    """Compute one permutation of the density dipole reduced
       bispectrum. Return zero if the reduced Gaunt factor is zero.

    Parameters
    ----------
    l1 : int
        Multipole.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    gsp : GeneralSpectra
        CosmoX GeneralSpectra instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.
    """
    l1, l2, l3 = _set_multipoles(l1, l2, l3)
    integtype = _set_integtype(integtype)
    ttype = gsp.kDensity
    ttype_G = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    gaunt = cosmox.geometry_reduced_Gaunt(l1, l2, l3)
    if (gaunt==0): # Total bispectrum not defined.
        return 0.0
    lp_grid = [l2-1, l2+1]
    lpp_grid = [l1-1, l1+1]
    res = sum(((2*lp+1) * (2*lpp+1) * cosmox.geometry_Q(l1, l2, l3, 1, lp, lpp)
               * (gsp.compute(lpp, l1, z3, z1, 1, ttype, ttype_G,
                              epsrel, epsabs, integtype)
                  * gsp.compute(lp, l2, z3, z2, -1, ttype, ttype_G,
                                epsrel, epsabs, integtype)
                  + gsp.compute(lpp, l1, z3, z1, -1, ttype, ttype_G,
                                epsrel, epsabs, integtype)
                  * gsp.compute(lp, l2, z3, z2, 1, ttype, ttype_G,
                                epsrel, epsabs, integtype)))
              for lp in lp_grid
              for lpp in lpp_grid)
    galbias = gsp.get_galaxy_bias()
    res *= galbias.b1(z3) / (16.*math.pi**2 * gaunt)
    if not linear_rsd:
        res *= galbias.b1(z1)*galbias.b1(z2) # gsp.kDensity has no bias.
    return res.real

@lru_cache()
def b_dens_quadrupole_oneperm(l1, l2, l3, z1, z2, z3, gsp,
                              linear_rsd=False, epsrel=1e-4,
                              epsabs=0., integtype='kMix'):
    """Compute one permutation of the density quadrupole reduced
       bispectrum. Return zero if the reduced Gaunt factor is zero.

    Parameters
    ----------
    l1 : int
        Multipole.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    gsp : GeneralSpectra
        CosmoX GeneralSpectra instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.

    """
    l1, l2, l3 = _set_multipoles(l1, l2, l3)
    integtype = _set_integtype(integtype)
    ttype = gsp.kDensity
    ttype_G = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    gaunt = cosmox.geometry_reduced_Gaunt(l1, l2, l3)
    if (gaunt==0): # Total bispectrum not defined.
        return 0.0
    lp_grid = [l2-2, l2, l2+2]
    lpp_grid = [l1-2, l1, l1+2]
    n = 0
    res = sum(((2*lp+1) * (2*lpp+1) * cosmox.geometry_Q(l1, l2, l3, 2, lp, lpp)
               * gsp.compute(lpp, l1, z3, z1, n, ttype, ttype_G,
                 epsrel, epsabs, integtype)
               * gsp.compute(lp, l2, z3, z2, n, ttype, ttype_G,
                 epsrel, epsabs, integtype))
              for lp in lp_grid
              for lpp in lpp_grid)
    galbias = gsp.get_galaxy_bias()
    res *= (galbias.b1(z3) + 7./2.*galbias.bS(z3)) / (42.*math.pi**2 * gaunt)
    if not linear_rsd:
        res *= galbias.b1(z1)*galbias.b1(z2) # gsp.kDensity has no bias.
    return res.real

@permute
def b_density(l1, l2, l3, z1, z2, z3, gsp, linear_rsd=False,
              epsrel=1e-4, epsabs=0., integtype='kMix'):
    """Compute density reduced bispectrum, including all permutations.

    Parameters
    ----------
    l1 : int
        Multipole.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    gsp : object
        Instance of cosmox.GeneralSpectra.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.

    """
    mono = b_dens_monopole_oneperm(l1, l2, l3, z1, z2, z3, gsp,
                                   linear_rsd=linear_rsd,
                                   epsrel=epsrel, epsabs=epsabs,
                                   integtype=integtype)
    dip = b_dens_dipole_oneperm(l1, l2, l3, z1, z2, z3, gsp,
                                linear_rsd=linear_rsd,
                                epsrel=epsrel, epsabs=epsabs,
                                integtype=integtype)
    quad = b_dens_quadrupole_oneperm(l1, l2, l3, z1, z2, z3, gsp,
                                     linear_rsd=linear_rsd,
                                     epsrel=epsrel, epsabs=epsabs,
                                     integtype=integtype)
    return mono + dip + quad
