# Copyright (C) 2018 Francesco Montanari.

# This file is part of byspectrum.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Velocity reduced bispectra."""

from functools import lru_cache
import cosmox

from .b_density import _set_integtype, _set_multipoles
from .permute import permute

@permute
@lru_cache()
def b_vv2(l1, l2, l3, z1, z2, z3, gsp, linear_rsd=False,
          epsrel=1e-4, epsabs=0., integtype='kMix'):
    """Velocity reduced bispectrum component given in Appendix A.2 of
    arXiv:1510.04202.

    Parameters
    ----------
    l1 : int
        Multipole.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    gsp : object
        CosmoX GeneralSpectra instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.
    """
    l1, l2, l3 = _set_multipoles(l1, l2, l3)
    integtype = _set_integtype(integtype)
    ttype_v = gsp.kVelocity
    ttype_v2 = gsp.kVelocity2
    ttype_G = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    res = (gsp.compute(l2, l2, z1, z2, 0, ttype_v2, ttype_G, epsrel,
                       epsabs, integtype)
           * gsp.compute(l3, l3, z1, z3, 0, ttype_v, ttype_G, epsrel,
                         epsabs, integtype)
           + gsp.compute(l2, l2, z1, z2, 0, ttype_v, ttype_G, epsrel,
                         epsabs, integtype)
           * gsp.compute(l3, l3, z1, z3, 0, ttype_v2, ttype_G, epsrel,
                         epsabs, integtype))
    galbias = gsp.get_galaxy_bias()
    if not linear_rsd:
        res *= galbias.b1(z2)*galbias.b1(z3) # gsp.kDensity has no bias.
    return res.real

@permute
@lru_cache()
def b_v1v1(l1, l2, l3, z1, z2, z3, gsp, linear_rsd=False,
          epsrel=1e-4, epsabs=0., integtype='kMix'):
    """Velocity reduced bispectrum component given in Appendix A.3 of
    arXiv:1510.04202.

    Parameters
    ----------
    l1 : int
        Multipole.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    gsp : object
        CosmoX GeneralSpectra instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.

    """
    l1, l2, l3 = _set_multipoles(l1, l2, l3)
    integtype = _set_integtype(integtype)
    ttype_v1 = gsp.kVelocity1
    ttype_G = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    res = (2. * gsp.compute(l2, l2, z1, z2, 0, ttype_v1, ttype_G, epsrel,
                       epsabs, integtype)
           * gsp.compute(l3, l3, z1, z3, 0, ttype_v1, ttype_G, epsrel,
                         epsabs, integtype))
    galbias = gsp.get_galaxy_bias()
    if not linear_rsd:
        res *= galbias.b1(z2)*galbias.b1(z3) # gsp.kDensity has no bias.
    return res.real

@permute
@lru_cache()
def b_vd1(l1, l2, l3, z1, z2, z3, gsp, linear_rsd=False,
          epsrel=1e-4, epsabs=0., integtype='kMix'):
    """Velocity reduced bispectrum component given in Appendix A.4 of
    arXiv:1510.04202.

    Parameters
    ----------
    l1 : int
        Multipole.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    gsp : object
        CosmoX GeneralSpectra instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.

    """
    l1, l2, l3 = _set_multipoles(l1, l2, l3)
    integtype = _set_integtype(integtype)
    ttype_v = gsp.kVelocity
    ttype_d1 = gsp.kDensity1
    ttype_G = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    galbias = gsp.get_galaxy_bias()
    res = (galbias.b1(z1) * gsp.compute(l2, l2, z1, z2, 0, ttype_d1, ttype_G,
                                        epsrel, epsabs, integtype)
           * gsp.compute(l3, l3, z1, z3, 0, ttype_v, ttype_G, epsrel,
                         epsabs, integtype)
           + gsp.compute(l2, l2, z1, z2, 0, ttype_v, ttype_G, epsrel,
                         epsabs, integtype)
           * galbias.b1(z1) * gsp.compute(l3, l3, z1, z3, 0, ttype_d1, ttype_G,
                                          epsrel, epsabs, integtype))
    if not linear_rsd:
        res *= galbias.b1(z2)*galbias.b1(z3) # gsp.kDensity has no bias.
    return res.real

@permute
@lru_cache()
def b_v1d(l1, l2, l3, z1, z2, z3, gsp, linear_rsd=False,
          epsrel=1e-4, epsabs=0., integtype='kMix'):
    """Velocity reduced bispectrum component given in Appendix A.5 of
    arXiv:1510.04202.

    Parameters
    ----------
    l1 : int
        Multipole.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    gsp : object
        CosmoX GeneralSpectra instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.

    """
    l1, l2, l3 = _set_multipoles(l1, l2, l3)
    integtype = _set_integtype(integtype)
    ttype_d = gsp.kDensity
    ttype_v1 = gsp.kVelocity1
    ttype_G = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    galbias = gsp.get_galaxy_bias()
    res = (galbias.b1(z1)
           * gsp.compute(l2, l2, z1, z2, 0, ttype_d, ttype_G, epsrel,
                       epsabs, integtype)
           * gsp.compute(l3, l3, z1, z3, 0, ttype_v1, ttype_G, epsrel,
                         epsabs, integtype)
           + galbias.b1(z1)
           * gsp.compute(l2, l2, z1, z2, 0, ttype_v1, ttype_G, epsrel,
                         epsabs, integtype)
           * gsp.compute(l3, l3, z1, z3, 0, ttype_d, ttype_G, epsrel,
                         epsabs, integtype))
    if not linear_rsd:
        res *= galbias.b1(z2)*galbias.b1(z3) # gsp.kDensity has no bias.
    return res.real
