# Copyright (C) 2018 Francesco Montanari.

# This file is part of byspectrum.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Lensing reduced bispectra."""

from functools import lru_cache
from math import sqrt

import cosmox

from .b_density import _set_integtype, _set_multipoles
from .permute import permute

@permute
@lru_cache()
def b_NdNpsi(l1, l2, l3, z1, z2, z3, gsp, linear_rsd=False,
          epsrel=1e-4, epsabs=0., integtype='kMix'):
    """Lensing reduced bispectrum component given in Appendix A.9 of
    arXiv:1510.04202.

    Parameters
    ----------
    l1 : int
        Multipole.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    gsp : object
        CosmoX GeneralSpectra instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.

    """
    l1, l2, l3 = _set_multipoles(l1, l2, l3)
    integtype = _set_integtype(integtype)
    ttype_k = gsp.kLensing
    ttype_d = gsp.kDensity
    ttype_G = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    galbias = gsp.get_galaxy_bias()
    sqrt_L3_L2 = sqrt(l3*(l3+1) / l2/(l2+1))
    res = (gsp.compute(l2, l2, z1, z2, 0, ttype_k, ttype_G, epsrel,
                       epsabs, integtype)
           * galbias.b1(z1) * gsp.compute(l3, l3, z1, z3, 0, ttype_d,
                                          ttype_G, epsrel, epsabs, integtype)
           * sqrt_L3_L2
           + galbias.b1(z1) * gsp.compute(l2, l2, z1, z2, 0, ttype_d,
                                          ttype_G, epsrel, epsabs, integtype)
           * gsp.compute(l3, l3, z1, z3, 0, ttype_k, ttype_G, epsrel,
                         epsabs, integtype)
           / sqrt_L3_L2)
    if not linear_rsd:
        res *= galbias.b1(z2)*galbias.b1(z3) # gsp.kDensity has no bias.
    res *= cosmox.geometry_A(l1, l2, l3)
    return res.real

@permute
@lru_cache()
def b_Nv1Npsi(l1, l2, l3, z1, z2, z3, gsp, linear_rsd=False,
          epsrel=1e-4, epsabs=0., integtype='kMix'):
    """Lensing reduced bispectrum component given in Appendix A.10 of
    arXiv:1510.04202.

    Parameters
    ----------
    l1 : int
        Multipoles.
    l2 : int
        Mutipole.
    l3 : int
        Mutipole.
    z1 : float
        Redshift.
    z2 : float
        Redshift.
    z3 : float
        Redshift.
    gsp : object
        CosmoX GeneralSpectra instance.
    linear_rsd : bool
        If True, add redshift-space distortions to linear
        density perturbations.
    epsrel : float
        Required integral relative tolerance.
    epsabs : float
        Required integral absolute tolerance. *Warning:* The absolute
        tolerance refers to the integral defined in the C++ code up to
        some pre-factor.
    integtype: str
        Integration strategy. Flags correspond to
        cosmox::GeneralSpectra::IntegType from C++ code.

    """
    l1, l2, l3 = _set_multipoles(l1, l2, l3)
    integtype = _set_integtype(integtype)
    ttype_k = gsp.kLensing
    ttype_v1 = gsp.kVelocity1
    ttype_G = gsp.kDensityG_kVelocity1 if linear_rsd else gsp.kDensity
    sqrt_L3_L2 = sqrt(l3*(l3+1) / l2/(l2+1))
    res = (gsp.compute(l2, l2, z1, z2, 0, ttype_k, ttype_G, epsrel,
                       epsabs, integtype)
           * gsp.compute(l3, l3, z1, z3, 0, ttype_v1, ttype_G, epsrel,
                         epsabs, integtype)
           * sqrt_L3_L2
           + gsp.compute(l2, l2, z1, z2, 0, ttype_v1, ttype_G, epsrel,
                         epsabs, integtype)
           * gsp.compute(l3, l3, z1, z3, 0, ttype_k, ttype_G, epsrel,
                         epsabs, integtype)
           / sqrt_L3_L2)
    galbias = gsp.get_galaxy_bias()
    if not linear_rsd:
        res *= galbias.b1(z2)*galbias.b1(z3) # gsp.kDensity has no bias.
    res *= cosmox.geometry_A(l1, l2, l3)
    return res.real
